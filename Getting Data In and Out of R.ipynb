{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting Data In and Out of R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading and Writing Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few principal functions reading data into R.\n",
    "\n",
    "- `read.table`, `read.csv`, for reading tabular data\n",
    "- `readLines`, for reading lines of a text file\n",
    "- `source`, for reading in R code files (`inverse` of `dump`)\n",
    "- `dget`, for reading in R code files (`inverse` of `dput`)\n",
    "- `load`, for reading in saved workspaces\n",
    "- `unserialize`, for reading single R objects in binary form"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are of course, many R packages that have been developed to read in all kinds of other datasets, and you may need to resort to one of these packages if you are working in a specific area."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are analogous functions for writing data to files\n",
    "\n",
    "- `write.table`, for writing tabular data to text files (i.e. CSV) or connections\n",
    "- `writeLines`, for writing character data line-by-line to a file or connection\n",
    "- `dump`, for dumping a textual representation of multiple R objects\n",
    "- `dput`, for outputting a textual representation of an R object\n",
    "- `save`, for saving an arbitrary number of R objects in binary format (possibly compressed) to a file.\n",
    "- `serialize`, for converting an R object into a binary format for outputting to a connection (or file)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading Data Files with `read.table()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `read.table()` function is one of the most commonly used functions for reading data. The help file for `read.table()` is worth reading in its entirety if only because thet function gets used a lot (run `?read.table` in R). I know, I know, everyone always says to read the help file, but this one is actually worth reading."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `read.table()` function has a few important arguments:\n",
    "\n",
    "- `file`, the name of a file, or a connection\n",
    "- `header`, logical indicating if the file has a header line\n",
    "- `sep`, a string indicating how the columns are separated\n",
    "- `colClasses`, a character vector indicating the class of each column in the dataset\n",
    "- `nrows`, the number of rows in the dataset. By default `read.table()` reads an entire file.\n",
    "- `comment.char`, a character string indicating the comment character. This defaults to \"`#`\". If there are no commented lines in your file, it's worth setting this to be the empty string \"\".\n",
    "- `skip`, the number of lines to skip from the beginning\n",
    "- `stringsAsFactors`, should character variables be coded as factors? This defaults to `TRUE` because back in the old days, if you had data that were stored as strings, it was because those strings represented levels of a categoical variable. Now we have lots of data that is text data and they don't always represent categorical variables. So you may want to set this to be `FALSE` in those cases. If you *always* want this to be `FALSE`,  you can set a global option via `options(stringsAsFactors = FALSE`. I've never seen so much heat generated on discussion forums about an R function argument than the `stringsAsFactors` argument. Seriously."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For small to moderately sized datasets, you can usually call `read.table` without specifying any other arguments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "data <- read.table(\"foo.txt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, R will automatically\n",
    "\n",
    "- skip lines that begin with `#`\n",
    "- figure out how many rows there are (and how much hmemory needs to be allocated)\n",
    "- figure what type of variable is in each column of the table."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Telling R all these things directly makes R run faster and more efficiently. The `read.csv()` function is identical to `read.table` except that some of the defaults are set differently (like the `sep` argument)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading in Larger Datasets with `read.table`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With much larger datasets, there are a few things that you can do that will make your life easier and will prevent R from choking.\n",
    "\n",
    "- Read the help page for `read.table`, which contains many hints\n",
    "- Make a rough calculation of the memory required to store your dataset (see the next section for an example of how to do this). If the dataset is larger than the amount of RAM on your computer, you can probably stop right here.\n",
    "- Set `comment.char = \"\"` if there are no commented lines in your file.\n",
    "- Use the `colClasses` argument. Specifying this option instead of using the default can make `read.table` run MUCH faster, often twice as fast. In order to use this option, you have to know the class of each column in your data frame. If all of the columns are \"numeric\", for example, then you can just set `colClasses = \"numeric\"`. A quick and dirty way to figure out the classes of each column is the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "initial <- read.table(\"datatable.txt\", nrows = 100)\n",
    "classes <- sapply(initial, class)\n",
    "tabAll <- read.table(\"datatable.txt\", colClasses = classes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Set `nrows`. This doesn't make R run faster but it helps with memory usage. A mild overestimate is okay. You can use the Unix tool `wc` to calculate the number of lines in a file.."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, when using R with larger datasets, it's also useful to know a few things about your system.\n",
    "\n",
    "- How much memory is available on your system?\n",
    "- What other applications are in use? Can you close any of them?\n",
    "- Are there other users logged into the same system?\n",
    "- What operating system are you using? Some operating systems can limit the amount of memory a single process can access."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating Memory Requirements for R Objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because R stores all of its objects physical memory, it is important to be cognizant of how much memory is being used up by all of the data objects residing in your workspace. One situation where it's particularly important to understand memory requirements is when you are reading in a new dataset into R. Fortunately, it's easy to make a back of the envelope calculation of how much memory will be required by a new dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, suppose I have a data frame with 1,500,000 rows and 120 columns, all of which are numeric data. Roughly, how much memory is required to store this data frame? Well on most modern computers, double precision floating point numbers are stored using 64 bits of memory, or 8 bytes. Given that information, you can do the following calculation:\n",
    "\n",
    "> $1,500,00 \\times 120 \\times 8 \\text{ bytes/numeric}$  \n",
    "> $= 1,440,000,000 \\text{ bytes}$  \n",
    "> $= 1,440,000,000 / 2^{20} \\text{ bytes/MB}$  \n",
    "> $= 1,373.29 \\text{ MB}$  \n",
    "> $= 1.34 \\text{ GB}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the dataset would require about $1.34 \\text{ GB of RAM}$. Most computers these days have at least that much RAM. However, you need to be aware of\n",
    "\n",
    "- what other programs might be running on your computer, using up RAM\n",
    "- what other R objects might already be taking up RAM in your workspace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reading in a large dataset for which you do not have enough RAM is one easy way to freeze up your computer (or at least your R session). This is usually unpleasant experience that usually requires you to kill the R process, in the best case scenario, or reboot your computer, in the worst case. So make sure you do a rough calculation of memory requirements before reading in a large dataset. You'll thank me later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the `readr` Package"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `readr` package is recently developed by Hadley Wickham to deal with reading in large flat files quickly. The package provides replacements for functions like `read.table()` and `read.csv()`. The analogous functions in `readr` are `read_table()` and `read_csv()`. These functions are often *much* faster than their base R analogues and provide a few other nice features such as progress meters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the most part, you can read use `read_table()` and `read_csv()` pretty much anywhere you might use `read.table()`. If there are non-fatal problems that occur while reading in the data, you will get a warning and the returned data frame will have some information about which rows/observations triggered the warning. This can be very helpful for \"debugging\" problems with your data before you get neck deep in data analysis."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
