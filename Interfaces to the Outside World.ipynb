{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interfaces to the Outside World"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data are read in using *connection* interfaces. Connections can be made to files (most common) or to other more exotic things.\n",
    "\n",
    "- `file`, opens a connection to a file\n",
    "- `gzfile`, opens a connection to a file compressed with gzip\n",
    "- `bzfile`, opens a connection to a file compressed with bzip2\n",
    "- `url`, opens a connection to a webpage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, connections are powerful tools that let you navigate files or other external objects. Connections can be thought of as a translator that lets you talk to objects that are outside of R. Those outside objects could be anything from a database, a simple text file, or a web service API. Connections allow R functions to talk to all these different external objects without you having to write custom code for each object."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## File Connections"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Connections to text files can be created with the `file()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "function (description = \"\", open = \"\", blocking = TRUE, encoding = getOption(\"encoding\"), \n",
      "    raw = FALSE, method = getOption(\"url.method\", \"default\"))  \n",
      "NULL\n"
     ]
    }
   ],
   "source": [
    "print(str(file))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `file()` function has a number of arguments that are common to many other connection functions so it's worth going into a little detail here.\n",
    "\n",
    "- `description` is the name of the file\n",
    "- `open` is a code indicating what mode the file should be opened in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `open` argument allows for the following options:\n",
    "\n",
    "- \"r\" open file in read only mode\n",
    "- \"w\" open a file for writing (and initializing a new file)\n",
    "- \"a\" open a file for appending\n",
    "- \"rb\", \"wb\", \"ab\" reading, writing, or appending in binary mode (Windows)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practice, we often don't need to deal with the connection interface directly as many functions for reading and writing data just deal with it in the background."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, if one were to explicitly use connections to read a CSV file into R, it might look like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Create a connection to 'foo.txt'\n",
    "con <- file(\"foo.txt\")\n",
    "\n",
    "## Open connection to 'foo.txt' in read-only mode\n",
    "open(con, \"r\")\n",
    "\n",
    "## Read from the connection\n",
    "data <- read.csv(con)\n",
    "\n",
    "## Close the connection\n",
    "close(con)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which is the same as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "data <- read.csv(\"foo.txt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the background, `read.csv()` opens a connection to the file `foo.txt`, reads from it, and closes the connection when its done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above example shows the basic approach to using connections. Connections must be opened, then they are read from or written to, and then they are closed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading Lines of a Text File"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Text files can be read line by line using the `readLines()` function. This function is useful for reading text files that may be unstructured or contain non-standard data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1] \"1000\"     \"10-point\" \"10th\"     \"11-point\" \"12-point\" \"16-point\"\n",
      " [7] \"18-point\" \"1st\"      \"2\"        \"20-point\"\n"
     ]
    }
   ],
   "source": [
    "## Open connection to gz-compressed text file\n",
    "con <- gzfile(\"words.gz\")\n",
    "x <- readLines(con, 10)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more structured text data like CSV files or tab-delimited files, there are other functions like `read.csv()` or `read.table()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above example used the `gzfile()` function which is used to create a connection to files compressed using the gzip algorithm. This approach is useful because it allows you to read from a file without having to uncompress the file first, which would be a waste of space and time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a complementary function `writeLines()` that takes a character vector and writes each element of the vector one line at a time to a text file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading From a URL Connection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `readLines()` function can be useful for reading in lines of webpages. Since web pages are basically text files that are stored on a remote server, there is conceptually not much difference between a web page and a local text file. However, we need R to negotiate the communication between your computer and the web server. This is what the `url()` function can do for you, by creating a `url` connection to a web server."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message in url(\"http://www.jhsph.edu\", \"r\"):\n",
      "\"InternetOpenUrl failed: 'The server name or address could not be resolved'\""
     ]
    },
    {
     "ename": "ERROR",
     "evalue": "Error in url(\"http://www.jhsph.edu\", \"r\"): cannot open the connection\n",
     "output_type": "error",
     "traceback": [
      "Error in url(\"http://www.jhsph.edu\", \"r\"): cannot open the connection\nTraceback:\n",
      "1. url(\"http://www.jhsph.edu\", \"r\")"
     ]
    }
   ],
   "source": [
    "## Open a URL connection for reading\n",
    "con <- url(\"http://www.jhsph.edu\", \"r\")\n",
    "\n",
    "## Read the web page\n",
    "x <- readLines(con)\n",
    "\n",
    "## Print out the first few lines\n",
    "print(head(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While reading in a simple web page is sometimes useful, particularly if data are embedded in the web page somewhere. However, more commonly we can use URL connection to read in specific data files that are stored on web servers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using URL connections can be useful for producing a reproducible analysis, because the code essentially documents where the data came from and how they were obtained. This is approach is preferable to opening a web browser and downloading a dataset by hand. Of course, the code you write with connections may not be executable at a later date if things on the server side are changed or reorganized."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
