{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using Textual and Binary Formats for Storing Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a variety of ways that data can be stored, including structured text files CSV or tab-delimited, or more complex binary formats. However, there is an intermediate format that is textual, but not as simple as something like CSV. The format is native to R and is somewhat readable because of its textual nature."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can create a more descriptive representation of an R objecct by using the `dput()` or `dump()` functions. The `dump()` and `dput()` functions are useful because the resulting textual format is editable, and in the case of corruption, potentially recoverable. Unlike writing out a table or CSV file, `dump()` and `dput()` preserve the *metadata* (sacrificing some readability), so that another user doesn't have to specify it all over again. For example, we can preserve the class of each column of a table or the levels of a factor variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Textual formats can work much better with version control programs like subversioni or git which can only track changes meaningfully in text files. In addition, textual formats can be longer-lived; if there is corruption somewhere in the file, it can be easier to fix the problem because one can just open the file in an editor and look at it (although this would probably only be done in a worst case scenario!). Finally, textual formats adhere to the Unix philosophy, if that means anything to you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few downsides to using these intermediate textual formats. The format is not very space-efficient, because all of the metadata is specified. Also, it is really only partially readable. In some instances it might be preferable to have data stored in a CSV file and then have a separate code file that specifies the meteadata."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using `dput()` and `dump()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One way to pass data around is by deparsing the R object with `dput()` and reading it back in (parsing it) using `dget()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "structure(list(a = 1, b = structure(1L, .Label = \"a\", class = \"factor\")), class = \"data.frame\", row.names = c(NA, \n",
      "-1L))\n",
      "  a b\n",
      "1 1 a\n"
     ]
    }
   ],
   "source": [
    "## Create a data frame\n",
    "y <- data.frame(a = 1, b = \"a\")\n",
    "## Print 'dput' output to console\n",
    "print(dput(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the `dput()` output is in the form of R code and that it preserves metadata like the class of the object, the row names, and the column names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output of `dput()` can also be saved directly to a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  a b\n",
      "1 1 a\n"
     ]
    }
   ],
   "source": [
    "## Send 'dput' output to a file\n",
    "dput(y, file = \"y.R\")\n",
    "## Read in 'dput' output from a file\n",
    "new.y <- dget(\"y.R\")\n",
    "print(new.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple objects can be deparsed at once using the dump function and read back in using `source`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "x <- \"foo\"\n",
    "y <- data.frame(a = 1L, b = \"a\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can `dump()` R objects to a file by passing a character vector of their names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "dump(c(\"x\", \"y\"), file = \"data.R\")\n",
    "rm(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The inverse of `dump()` is `source()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "'data.frame':\t1 obs. of  2 variables:\n",
      " $ a: int 1\n",
      " $ b: Factor w/ 1 level \"a\": 1\n",
      "NULL\n"
     ]
    }
   ],
   "source": [
    "source(\"data.R\")\n",
    "print(str(y))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"foo\"\n"
     ]
    }
   ],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binary Formats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The complement to the textual format is the binary format, which is sometimes necessary to use for efficiency purposes, or because there's just no useful way to represent data in a textual manner. Also, with numeric data, one can often lose precision when converting to and from a textual format, so it's better to stick with a binary format."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key functions for converting R objects into a binary format are `save()`, `save.image()`, and `serialize()`. Individual R objects can be saved to a file using the `save()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "a <- data.frame(x = rnorm(100), y = runif(100))\n",
    "b <- c(3, 4.4, 1 / 3)\n",
    "\n",
    "## Save 'a' and 'b' to a file\n",
    "save(a, b, file = \"mydata.rda\")\n",
    "\n",
    "## Load 'a' and 'b' into your workspace\n",
    "load(\"mydata.rda\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have a lot of objects that you want to save to a file, you can save all objects in your workspace using the `save.image()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Save everything to a file\n",
    "save.image(file = \"mydata.RData\")\n",
    "\n",
    "## Load all objects in this file\n",
    "load(\"mydata.RData\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that I've used the `.rda` extension when using `save()` and the `.RData` extension when using `save.image()`. This is just my personal preference; you can use whatever file extension you want. The `save()` and `save.image()` functions do not care. However, `.rda` and `.RData` are fairly common extensions and you may want to use them because they are recognized by other software."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `serialize()` function is used to convert individual R objects into a binary format that can be communicated across an arbitrary connection. This may get sent to a file, but it could get sent over a network or other connection."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you call `serialize()` on an R object, the output will be a raw vector coded in hexadecimal format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " [1] 58 0a 00 00 00 02 00 03 05 01 00 02 03 00 00 00 00 13 00 00 00 03 00 00 00\n",
      "[26] 0e 00 00 00 01 3f f0 00 00 00 00 00 00 00 00 00 0e 00 00 00 01 40 00 00 00\n",
      "[51] 00 00 00 00 00 00 00 0e 00 00 00 01 40 08 00 00 00 00 00 00\n"
     ]
    }
   ],
   "source": [
    "x <- list(1, 2, 3)\n",
    "print(serialize(x, NULL))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want, this can be sent to a file, but in that case you are better off using something like `save()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The benefit of the `serialize()` function is that it is the only way to perfectly represent an R object in an exportable format, without losing precision or any metadata. If that is what you need, then `serialize()` is the function for you."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
